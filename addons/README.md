Add-ons folder
==============

This is the folder in which Mootivated Add-ons should be installed.

Available hooks
---------------

### extend_block_mootivated_content

Allows to inject content after the block generated its own content. This is useful to inject JavaScript.

```php
/**
 * @param manager $manager The manager.
 * @param school $school The school.
 * @param renderer $renderer The block's renderer.
 * @return string|null
 */
function mootivatedaddon_pluginname_extend_block_mootivated_content($manager, $school, $renderer);

```
